# Battery capacity loger

Simple Matlab script which monitors the battery discharge by using active load Chroma 63800 series 

Command list
syst:set:mode dc - ustawienie trybu DC

- syst:set:mode? - odczytu trybu

- [load:]mode curr - załadowanie trybu stały prąd

- [load:]mode? - odczyt aktulanego trybu

- [load:]curr[:lev][:ampl]:dc 2 - ustawienie wartości obciążenia lub curr 1

- [load:]curr[:lev][:ampl][:dc]? - odczytu prądu obciązenia lub curr?

- load on - włączenie