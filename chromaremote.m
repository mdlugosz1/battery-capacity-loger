close all;
clear all;

serial_port.Name = 'COM7';
serial_port.Speed = 56700;

chroma.current_amp = '1.0';
chroma.current_amp_max = '1.5';

chroma.power_amp = 200;

voltage_min = 11.5; % V

messure_freq = 2; % Hz

% Handler to serrial port
s = serialport(serial_port.Name, serial_port.Speed, "Timeout", 10);

writeline(s, "*idn?");
summary = "Podłączono do urządzenia: " + readline(s);

%%  Konfigurujemy urzadzenie

% Ustawiamy tryb DC
writeline(s, "syst:set:mode dc");

% Ustawiamy tryb stały prąd
writeline(s, "[load:]mode curr");

% Ustawiamy wartosc prąd
writeline(s, "[load:]curr[:lev][:ampl]:dc " + chroma.current_amp);

% Ustawiamy tryb stala moc
% writeline(s, "[load:]mode pow");

% Ustawiamy wartosc mocy
% writeline(s, "[load:]pow[:lev][:ampl]:dc " + chroma.power_amp);

%% Zabezpiecznie proadowe
% Ustawiamy maksymlaną wartość prądu
writeline(s, "[load:]curr:max[:lev][:ampl]:dc " + chroma.current_amp_max);

%% Odczytanie ustawien
writeline(s, "syst:set:mode?");
summary = summary + newline + "Typ: " + readline(s);

writeline(s, "[load:]mode?");
summary = summary + newline + "Tryb pracy: " + readline(s);

writeline(s, "[load:]curr[:lev][:ampl][:dc]?");
summary = summary + newline + "Prąd źródła: " + readline(s) + " A";

writeline(s, "[load:]curr:max[:lev][:ampl]:dc?");
summary = summary + newline + "Maksymalny prąd źródła: " + readline(s) + " A";

summary = summary + newline + "Minimalne napięcie źródła: " + string(voltage_min) + " V";

answer = questdlg(summary + newline + "Rozpocząć pomiar?", ...
	'Menu', ...
	'Tak', 'Nie', 'Nie');

if answer=='Tak'
   disp("Rozpoczynam pomiar..."); 
else 
    return
end

%% GUI
figure1 = figure;
gui.stop = uicontrol('Style', 'PushButton', ...
                    'String', 'Stop', ...
                    'Callback', 'stopExp=true;');

gui.save = uicontrol('Style', 'PushButton', ...
                    'String', 'Zapisz wyniki', ...
                    'Position', [20 20 100 20], ....
                    'Callback', @callbackSaveData);                

gui.v_val = uicontrol('Style', 'edit', 'String', '0.00', 'Enable', 'inactive', ...
    'Position', [20 20 80 20], 'FontSize', 9, 'FontWeight', 'bold');

gui.i_val = uicontrol('Style', 'edit', 'String', '0.00', 'Enable', 'inactive', ...
    'Position', [20 20 80 20], 'FontSize', 9, 'FontWeight', 'bold');

gui.p_val = uicontrol('Style', 'edit', 'String', '0.00', 'Enable', 'inactive', ...
    'Position', [20 20 80 20], 'FontSize', 9, 'FontWeight', 'bold');

gui.ah_val = uicontrol('Style', 'edit', 'String', '0.00', 'Enable', 'inactive', ...
    'Position', [20 20 120 20], 'FontSize', 9, 'FontWeight', 'bold');

align([gui.stop gui.save gui.v_val gui.i_val gui.p_val gui.ah_val],'fixed',10,'bottom');

gui.v_ax = axes('Parent',figure1,'Position',[0.05 0.55 0.9 0.4 ]);
grid;hold on;
axis([0 inf 10 13]);
ylabel(gui.v_ax, 'Napięcie [V]');

gui.i_ax = axes('Parent',figure1,'Position',[0.05 0.1 0.9 0.4 ])
grid;hold on;
axis([0 inf 0 str2double(chroma.current_amp_max)]);
ylabel(gui.i_ax, 'Prąd [A]');

%% Dane zmierzone
voltage = 0.0;
current = 0.0;
power = 0.0;

% Pierwszy pomiar
writeline(s, 'meas:volt?');
voltage = str2double(readline(s));
gui.v_line = line(gui.v_ax, 0, voltage);

writeline(s, 'meas:curr?');
current = str2double(readline(s));
gui.i_line = line(gui.i_ax, 0, current);

% Wszystkie pomiary
global data_out
data_out = [];

%% Start load
writeline(s, "load on");

stopExp = false;

% Ustawiamy staly czas wykonywania pentli
r = rateControl(messure_freq);
reset(r);
data_count = 0;
while ~stopExp
    
    t_tmp = r.TotalElapsedTime;
    
    % Sprawdzamy czy urzodzenie pracuje poprawnie
    % Status of Operation , 
    writeline(s, 'stat:oper:even?');
    operation_status = str2num(readline(s));
    if operation_status>0
        errorStatus(operation_status);
        break;
    end
    
    % Pomiar napiecia
    writeline(s, 'meas:volt?');
    voltage = str2double(readline(s));
    gui.v_val.String = "U= " + string(voltage) + " V";
    
    if voltage < voltage_min
        warning("Niskie napięcie źródła.");
        break;
    end
    
    % Pomiar pradu
    writeline(s, 'meas:curr?');
    current = str2double(readline(s));
    gui.i_val.String = "I= " + string(current) + " A";
    
    % Pomiar mocy
    writeline(s, 'meas:pow?');
    power = str2double(readline(s));
    gui.p_val.String = "P= " + string(power) + " W";
    
    % Wyliczenie Ah
    gui.ah_val.String = "C= " + string( ((data_count/messure_freq)/3600) * current ) + " Ah";
    
    % Wykres napiecie
    gui.v_line.XData = [ gui.v_line.XData gui.v_line.XData(end)+1]; 
    gui.v_line.YData = [ gui.v_line.YData voltage]; 
    
    % Wykres prad
    gui.i_line.XData = [ gui.i_line.XData gui.i_line.XData(end)+1]; 
    gui.i_line.YData = [ gui.i_line.YData current]; 
    
    data_out = [data_out; t_tmp voltage current power];
    
    data_count = data_count+1;
    waitfor(r);
end

%% Stop load
writeline(s, "load off");

%% Save data
saveData(data_out);

%% Zamykamy łącze szeregowe
clear s fid path file time current voltage power serial_port answer  r summary stopExp t_tmp chroma

function callbackSaveData(hObject, eventdata, handles)
    global data_out
    saveData(data_out);
end

function saveData(data)
    [file,path] = uiputfile('*.csv');
    if ischar(file)
    
        fid = fopen( fullfile(path,file), 'w'); 
        fprintf(fid,'%s\n', 'time,U,I,P');
        fclose(fid);

        dlmwrite(fullfile(path,file), data, '-append');
    end
end

function errorStatus(status)
    if bitget(status, 1)
        warning("OT: Load fail");
    end
    if bitget(status, 2)
        warning("LF: Over temperature protection");
    end
    if bitget(status, 3)
        warning("FF: Fan fail");
    end
end

% https://www.mathworks.com/matlabcentral/answers/396570-how-can-i-plot-a-continuous-graphic-of-live-recorded-data-with-arduino
% https://www.mathworks.com/help/robotics/ref/ratecontrol.html